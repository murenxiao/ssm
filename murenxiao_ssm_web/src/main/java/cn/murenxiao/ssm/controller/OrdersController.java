package cn.murenxiao.ssm.controller;

import cn.murenxiao.domain.Orders;
import cn.murenxiao.service.OrdersService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.RolesAllowed;
import java.util.List;

/**
 * @Title: OrdersController
 * @Description:cn.murenxiao.ssm.controller
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/3/1 11:55
 */
@Controller
@RequestMapping("/orders")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

   /* @RequestMapping("/findAll.do")
    private ModelAndView findAll() throws Exception {
        ModelAndView modelAndView=new ModelAndView();
        List<Orders> ordersList = ordersService.findAll();
        System.out.println("==================");
        for (Orders orders : ordersList) {
            System.out.println(orders);
        }
        System.out.println("==================");
        modelAndView.addObject("ordersList",ordersList);
        modelAndView.setViewName("orders-list");
        return modelAndView;
    }*/

    /**
     * 查看所有的订单
     * @param page
     * @param size
     * @return
     * @throws Exception
     */

   @RequestMapping("/findAll.do")
   @Secured("ROLE_ADMIN")//注意必须加前缀！！！！！
   public ModelAndView findAll(@RequestParam(name = "page", required = true, defaultValue = "1") Integer page, @RequestParam(name = "size", required = true, defaultValue = "4") Integer size) throws Exception {
       ModelAndView mv = new ModelAndView();
       List<Orders> ordersList = ordersService.findAll(page, size);
       //PageInfo就是一个分页Bean
       PageInfo pageInfo=new PageInfo(ordersList);
       mv.addObject("pageInfo",pageInfo);
       mv.setViewName("orders-list");
       return mv;
   }


    /**
     *查看订单详情
     * @param ordersId
     * @return
     */
   @RequestMapping("/findById.do")
    public ModelAndView findById(@RequestParam(name = "id" ,required = true) String ordersId){
       ModelAndView mv = new ModelAndView();
       Orders orders=ordersService.findById(ordersId);
       mv.addObject("orders",orders);
       mv.setViewName("orders-show");
       return mv;
   }
}
