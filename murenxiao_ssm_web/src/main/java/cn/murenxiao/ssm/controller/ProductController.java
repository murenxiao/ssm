package cn.murenxiao.ssm.controller;

import cn.murenxiao.domain.Product;
import cn.murenxiao.service.ProductService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.RolesAllowed;
import java.util.List;

/**
 * @Title: ProductController
 * @Description:cn.murenxiao.ssm.controller
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/2/26 10:49
 */
@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;



    @RequestMapping("/save.do")
    public String save(Product product) throws Exception {
        System.out.println("*****************************************************");
        System.out.println(product);
        System.out.println("*****************************************************");
        productService.save(product);
        return "redirect:findAll.do";
    }

    /**
     * 查询全部产品
     * @return
     * @throws Exception
     */
    @RequestMapping("/findAll.do")
    @RolesAllowed("ADMIN")//只有拥有admin权限的才可以访问这个方法   jsr250  可以省略前缀
    public ModelAndView findAll(@RequestParam(name = "page", required = true, defaultValue = "1") Integer page, @RequestParam(name = "size", required = true, defaultValue = "4") Integer size) throws Exception {
        ModelAndView mv=new ModelAndView();

        List<Product> ps = productService.findAll(page,size);
        //PageInfo就是一个分页Bean
        PageInfo pageInfo=new PageInfo(ps);
        mv.addObject("pageInfo",pageInfo);
        mv.setViewName("product-list");
        return mv;
    }
}
