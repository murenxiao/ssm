package cn.murenxiao.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @Title: BCryptPasswordEncoderUtils
 * @Description:cn.murenxiao.utils
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/3/3 16:40
 */
public class BCryptPasswordEncoderUtils {
    private static BCryptPasswordEncoder bCryptPasswordEncoder=new BCryptPasswordEncoder();
    public static String encodePassword(String password){
        return bCryptPasswordEncoder.encode(password);
    }

    public static void main(String[] args) {
        String password="19981121";
        String pwd = encodePassword(password);
        //$2a$10$tJHudmJh6MRPdiL7mv0yfe0nZJbDHuhl7sSTnqNC4DauMik9ppi4K
        //$2a$10$Ce8LB3jdYDZ2f6HB281zA.4eC7v6ziJdK8MMWg0Yu8ETMg5ToMpIe
        System.out.println(pwd);
        System.out.print(pwd.length());
    }
}
