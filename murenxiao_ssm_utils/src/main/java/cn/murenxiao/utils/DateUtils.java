package cn.murenxiao.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Title: DateUtils
 * @Description:cn.murenxiao.utils
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/2/27 14:58
 */
public class DateUtils {
    /**
     * 日期转换成字符串
     */
    public static  String date2String(Date date,String ptt){
        SimpleDateFormat sdf=new SimpleDateFormat();
        String format = sdf.format(date);
        return format;
    }

    public static Date String2Date(Date date, String ptt) throws ParseException {
        SimpleDateFormat sdf=new SimpleDateFormat();
        Date parse = sdf.parse(ptt);
        return parse;
    }
}
