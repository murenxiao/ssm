package cn.murenxiao.dao;

import cn.murenxiao.domain.Product;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Title: ProductDao
 * @Description:cn.murenxiao.dao
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/2/26 10:06
 */
public interface ProductDao {


    /**
     * 更具id查询
     * @param id
     * @return
     */
    @Select("select * from product where id=#{id}")
    Product findById(String id) throws  Exception;

    /**
     * 查询所有的产品信息
     * @return
     */
    @Select("select * from product")
    List<Product> findAll() throws Exception;


    @Insert("insert into product(productNum,productName,cityName,departureTime,productPrice,productDesc,productStatus)  " +
            "values(#{productNum},#{productName},#{cityName},#{departureTime},#{productPrice},# {productDesc},#{productStatus})")
    void save(Product product);
}
