package cn.murenxiao.dao;

import cn.murenxiao.domain.Role;
import cn.murenxiao.domain.UserInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @Title: UserDao
 * @Description:cn.murenxiao.dao
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/3/2 20:29
 */
public interface UserDao {

    @Select("select * from users where username=#{username}")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "username", column = "username"),
            @Result(property = "email", column = "email"),
            @Result(property = "password", column = "password"),
            @Result(property = "phoneNum", column = "phoneNum"),
            @Result(property = "status", column = "status"),
            @Result(property = "roles",column = "id",javaType = java.util.List.class,many = @Many(select = "cn.murenxiao.dao.RoleDao.findRoleByUserId"))
    })
    public UserInfo findByUsername(String username);


    @Select("select * from users")
    List<UserInfo> findAll();



    @Insert("insert into users(email,username,password,phoneNum,status)values(#{email},#{username},#{password},#{phoneNum},#{status})")
    //@Insert("insert into users(email,username,password,phoneNum,status)values(#{email},#{username},#{password},#{phoneNum},#{status})")
    //记录bug  在在这里写sql 语句时#与{}中间多了一个空格  导致出错
    void save(UserInfo userInfo) throws Exception;


    @Select("select * from users where id=#{id}")
    @Results({
            @Result(id = true, property = "id", column = "id"),
            @Result(property = "username", column = "username"),
            @Result(property = "email", column = "email"),
            @Result(property = "password", column = "password"),
            @Result(property = "phoneNum", column = "phoneNum"),
            @Result(property = "status", column = "status"),
            @Result(property = "roles",column = "id",javaType = java.util.List.class,many = @Many(select = "cn.murenxiao.dao.RoleDao.findRoleByUserId"))
    })
    UserInfo findById(String id);


    @Select("select * from role where id not  in (select roleId from users_role where userId=#{userId})")
    List<Role> findOtherRoles(String userId);


    @Insert("insert into users_role(userId,roleId) values(#{userId},#{roleId})")
    void addRoleToUser(@Param("userId") String userId, @Param("roleId") String roleId);
}
