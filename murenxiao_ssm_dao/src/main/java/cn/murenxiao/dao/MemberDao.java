package cn.murenxiao.dao;

import cn.murenxiao.domain.Member;
import org.apache.ibatis.annotations.Select;

/**
 * @Title: MemberDao
 * @Description:cn.murenxiao.dao
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/3/1 21:07
 */
public interface MemberDao {
    @Select("select * from member where id=#{id}")
    Member findById(String id);
}
