package cn.murenxiao.dao;

import cn.murenxiao.domain.Member;
import cn.murenxiao.domain.Orders;
import cn.murenxiao.domain.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @Title: OrdersDao
 * @Description:cn.murenxiao.dao
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/3/1 11:50
 */
public interface OrdersDao {
    /**
     * 查询所有订单
     * @return
     */
    @Select("select * from orders")
    @Results({
            @Result(id=true,property = "id",column = "id"),
            @Result(property = "orderNum",column = "orderNum"),
            @Result(property = "orderTime",column = "orderTime"),
            @Result(property = "orderStatus",column = "orderStatus"),
            @Result(property = "peopleCount",column = "peopleCount"),
            @Result(property = "payType",column = "payType"),
            @Result(property = "orderDesc",column = "orderDesc"),
            @Result(property = "product",column = "productId",javaType = Product.class,one = @One(select ="cn.murenxiao.dao.ProductDao.findById")),
    })
    public List<Orders> findAll();


    /**
     * 根据id查询订单
     * @param ordersId
     * @return
     * @throws Exception
     */
    @Select("select * from orders where id=#{ordersId}")
    @Results({
            @Result(id=true,property = "id",column = "id"),
            @Result(property = "orderNum",column = "orderNum"),
            @Result(property = "orderTime",column = "orderTime"),
            @Result(property = "orderStatus",column = "orderStatus"),
            @Result(property = "peopleCount",column = "peopleCount"),
            @Result(property = "payType",column = "payType"),
            @Result(property = "orderDesc",column = "orderDesc"),
            @Result(property = "product",column = "productId",javaType = Product.class,one = @One(select ="cn.murenxiao.dao.ProductDao.findById")),
            @Result(property = "member",column = "memberId",javaType = Member.class,one =@One(select ="cn.murenxiao.dao.MemberDao.findById")),
            @Result(property = "travellers",column ="id",javaType = java.util.List.class,many = @Many(select = "cn.murenxiao.dao.TravellerDao.findByOrdersId"))
    })
    Orders findById(String ordersId);
}
