package cn.murenxiao.dao;

import cn.murenxiao.domain.Traveller;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Title: TravellerDao
 * @Description:cn.murenxiao.dao
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/3/2 7:46
 */
public interface TravellerDao {
    @Select("select * from traveller where id in (select travellerId from order_traveller where orderId=#{ordersId})")
    List<Traveller> findByOrdersId(String ordersId) throws Exception;
}
