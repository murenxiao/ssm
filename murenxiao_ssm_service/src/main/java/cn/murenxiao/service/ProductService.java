package cn.murenxiao.service;

import cn.murenxiao.domain.Product;

import java.util.List;

/**
 * @Title: ProductService
 * @Description:cn.murenxiao.service
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/2/26 10:18
 */
public interface ProductService {

    List<Product> findAll(int page,int size) throws Exception;

    void save(Product product) throws Exception;
}
