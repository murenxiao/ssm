package cn.murenxiao.service.impl;

import cn.murenxiao.dao.OrdersDao;
import cn.murenxiao.domain.Orders;
import cn.murenxiao.service.OrdersService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Title: OrdersServiceImpl
 * @Description:cn.murenxiao.service.impl
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/3/1 11:53
 */
@Service
@Transactional
public class OrdersServiceImpl implements OrdersService {
    @Autowired
    private OrdersDao ordersDao;

    @Override
    public List<Orders> findAll(int page, int size) throws Exception {
        //参数pageNum 是页码值   参数pageSize 代表是每页显示条数
        PageHelper.startPage(page, size);
        return ordersDao.findAll();
    }

    @Override
    public Orders findById(String ordersId) {
        return ordersDao.findById(ordersId);
    }
}
