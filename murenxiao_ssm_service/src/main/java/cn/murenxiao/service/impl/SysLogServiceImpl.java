package cn.murenxiao.service.impl;

import cn.murenxiao.dao.SysDao;
import cn.murenxiao.domain.SysLog;
import cn.murenxiao.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Title: SysLogServiceImpl
 * @Description:cn.murenxiao.service.impl
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/3/7 18:32
 */
@Service
@Transactional
public class SysLogServiceImpl implements SysLogService {
    @Autowired
    SysDao sysDao;

    @Override
    public void save(SysLog sysLog) {
        sysDao.save(sysLog);
    }

    @Override
    public List<SysLog> findAll() {
        return sysDao.findAll();
    }
}
