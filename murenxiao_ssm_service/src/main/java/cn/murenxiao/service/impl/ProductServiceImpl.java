package cn.murenxiao.service.impl;

import cn.murenxiao.dao.ProductDao;
import cn.murenxiao.domain.Product;
import cn.murenxiao.service.ProductService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Title: ProductServiceImpl
 * @Description:cn.murenxiao.service.impl
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/2/26 10:22
 */

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    @Override
    public List<Product> findAll(int page,int size) throws Exception {
        PageHelper.startPage(page, size);
        return productDao.findAll();
    }

    @Override
    public void save(Product product) throws Exception {
        productDao.save(product);
    }
}
