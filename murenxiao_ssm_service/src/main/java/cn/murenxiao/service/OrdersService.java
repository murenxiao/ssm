package cn.murenxiao.service;

import cn.murenxiao.domain.Orders;

import java.util.List;

/**
 * @Title: OrdersService
 * @Description:cn.murenxiao.service
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/3/1 11:42
 */
public interface OrdersService {
    List<Orders> findAll(int page, int size) throws Exception;

    Orders findById(String ordersId);
}
