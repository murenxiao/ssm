package cn.murenxiao.service;

import cn.murenxiao.domain.SysLog;

import java.util.List;

/**
 * @Title: SysLogService
 * @Description:cn.murenxiao.service
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/3/7 18:31
 */
public interface SysLogService {
    void save(SysLog sysLog);

    List<SysLog> findAll();

}
