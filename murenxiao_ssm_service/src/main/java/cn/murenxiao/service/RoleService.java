package cn.murenxiao.service;

import cn.murenxiao.domain.Permission;
import cn.murenxiao.domain.Role;

import java.util.List;

/**
 * @Title: RoleService
 * @Description:cn.murenxiao.service
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/3/4 23:11
 */
public interface RoleService {
    List<Role> findAll();

    void save(Role role);

    Role findById(String roleId);

    List<Permission> findOtherPermissions(String roleId);

    void addPermissionToRole(String roleId, String[] permissionIds);
}
