package cn.murenxiao.service;

import cn.murenxiao.domain.Role;
import cn.murenxiao.domain.UserInfo;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 * @Title: UserService
 * @Description:cn.murenxiao.service
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/3/2 19:59
 */
public interface UserService extends UserDetailsService {

    List<UserInfo> findAll(int page,int size);

    void save(UserInfo userInfo) throws Exception;

    UserInfo findById(String id);

    List<Role> findOtherRoles(String userId);

    void addRoleToUser(String userId, String[] roleIds);

}
