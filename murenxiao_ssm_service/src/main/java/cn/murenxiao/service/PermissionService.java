package cn.murenxiao.service;

import cn.murenxiao.domain.Permission;

import java.util.List;

/**
 * @Title: PermissionService
 * @Description:cn.murenxiao.service.impl
 * @Auther: 木人小
 * @Version: 1.0
 * @create 2020/3/5 12:04
 */
public interface PermissionService {
    List<Permission> findAll();

    void save(Permission permission);

}
